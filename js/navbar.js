const hamburguer = document.querySelector('.hamb');
const nav = document.querySelector('header .container nav');
const hambIcono = document.querySelector('.hamb i');
//querySelectorAll selecciona varios elementosdel DOM
const navLink = document.querySelectorAll('header .container nav a');

hamburguer.addEventListener('click', (e) => {
  console.log(e.target);

  //Cancela el evento relacionado
  e.preventDefault();
   /*
  classList accede a la lista de clases de un elemento 
  toggle cuando sólo hay un argumento presente alterna el valor de la clase por ejemplo: si la clase existe la elimina y devuelve false, si no la añade y devuelve true
  */
  nav.classList.toggle('open');
  // Adicionando icono de google fonts
  hambIcono.classList.toggle('fa-xmark');
});

navLink.forEach(enlace => {
  enlace.addEventListener('click', () => {
     /*
  classList accede a la lista de clases de un elemento 
  toggle cuando sólo hay un argumento presente alterna el valor de la clase por ejemplo: si la clase existe la elimina y devuelve false, si no la añade y devuelve true
  */
  nav.classList.toggle('open');
  // Adicionando icono de google fonts
  hambIcono.classList.toggle('fa-xmark');
  });
});